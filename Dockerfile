#
# Build stage
#
FROM golang:1.15-alpine as build

# build root
WORKDIR /build

COPY . .

# build & test
RUN apk add --no-cache git upx ca-certificates \
    && go get github.com/elazarl/goproxy \
    && go get github.com/rookmoot/proxifier/logger \
    && CGO_ENABLED=0 GOOS=linux go build main.go \
    && upx --ultra-brute main

#
# Final image
#
FROM scratch

# copy binary and ca certs
COPY --from=build /build/main /bin/proxy

COPY proxy_prod.json /proxy.json

EXPOSE 8080

ENV PROXY_PATH /proxy.json

ENTRYPOINT ["/bin/proxy"]
