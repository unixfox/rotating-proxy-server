package main

import (
	"net"
	"net/http"
	"net/url"
	"flag"
	"os"

	"./forward"
	"./proxy"
	"github.com/rookmoot/proxifier/logger"
	"github.com/elazarl/goproxy"
)

var (
	log = logger.ColorLogger{}
	PROXY_PATH = "/home/emilien/Documents/rotating-proxy-server/proxy_dev.json"
)

type SimpleHandler struct {
	M *proxy.Manager
}

func (t *SimpleHandler) handleRequest(conn net.Conn) {
	if (log.Verbose) {
		log.Info("New client connected")
	}

	fwd, err := forward.New(conn, log, t.M.GetNumberOfProxy())
	if err != nil {
		log.Warn("%v", err)
		return
	}
	defer fwd.Close()

	fwd.OnSelectRemote(func(req *http.Request) (forward.Remote, error) {
		return t.M.GetProxy()
	})

	err = fwd.Forward()
	if err != nil {
		log.Warn("%v", err)
	}
}

func main() {
	log.Info("Now listening to proxy connections!")

	verbose := flag.Bool("v", false, "should every proxy request be logged to stdout")
	log.Verbose = *verbose
	log.Color = true

	proxyGo := goproxy.NewProxyHttpServer()
	proxyGo.Verbose = *verbose
	proxyGo.OnRequest().HandleConnect(goproxy.AlwaysMitm)
	proxyGo.OnRequest().DoFunc(
		func(req *http.Request,ctx *goproxy.ProxyCtx)(*http.Request,*http.Response) {
			if req.URL.Scheme == "https" {
				req.URL.Scheme = "http"
				req.Header.Add("X-Proxifier-Https", "On");
			}
			return req, nil
		})
	proxyGo.Tr.Proxy = func(req *http.Request) (*url.URL, error) {
		return url.Parse("http://localhost:8081")
	}
	connectReqHandler := func(req *http.Request) {}
	proxyGo.ConnectDial = proxyGo.NewConnectDialToProxyWithHandler("http://localhost:8081", connectReqHandler)
	go http.ListenAndServe(":8080", proxyGo)

	proxyManager, err := proxy.NewManager(log)

	if err != nil {
		panic(err)
	}

	proxyPath, ok := os.LookupEnv("PROXY_PATH")
	if ok {
		PROXY_PATH = proxyPath
	}

	proxyManager.UpdateProxies(PROXY_PATH)

	t := SimpleHandler{
		M: proxyManager,
	}

	addr, err := net.ResolveTCPAddr("tcp", "localhost:8081")
	if err != nil {
		panic(err)
	}

	listener, err := net.ListenTCP("tcp", addr)
	if err != nil {
		panic(err)
	}
	defer listener.Close()

	for {
		conn, err := listener.AcceptTCP()
		if err != nil {
			log.Warn("%v", err)
		}

		go t.handleRequest(conn)
	}
}
