package proxy

import (
	"net"
	"fmt"
)

type Proxy struct {
	id int
	addr *net.TCPAddr
	infos map[string]string
}

func (p *Proxy)GetAddress() string {
	return fmt.Sprintf("%s:%v", p.infos["ipaddress"], p.infos["port"])
}

func (p *Proxy)GetRemoteAddr() (*net.TCPAddr, error) {
	return p.addr, nil
}
