package proxy

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math/rand"
	"strings"
	"time"
	"net"

	"github.com/rookmoot/proxifier/logger"
)

type Manager struct {
	log     logger.Logger
	proxies []*Proxy
}

func proxyExist(a *Proxy, list []*Proxy) bool {
    for _, b := range list {
        if b == a {
            return true
        }
    }
    return false
}

func NewManager(log logger.Logger) (*Manager, error) {
	m := Manager{
		log: log,
	}

	// err := m.loadProxyList()
	// if err != nil {
	// 	return nil, err
	// }

	return &m, nil
}

func (m *Manager) UpdateProxies(filepath string) error {
	err := m.readProxiesFromFile(filepath)
	if err != nil {
		return err
	}

	// m.log.Info("%v", proxies[0])

	// m.proxies := make(proxies)
	// for _, proxy := range proxies {
	// 	if proxy.GetAnonymityLevel() == "elite" && (proxy.GetProtocol() == "http" || proxy.GetProtocol() == "https") {
	// 		m.log.Info("proxy: %v (%v, %v)", proxy.GetAddress(), proxy.GetProtocol(), proxy.GetAnonymityLevel())
	// 		m.proxies = append(m.proxies, &proxy)
	// 	}
	// 	m.log.Info("%v", m.proxies[0])
	// }

	ticker := time.NewTicker(60 * time.Second)
	quit := make(chan struct{})
	go func() {
		for {
		   select {
			case <- ticker.C:
				m.readProxiesFromFile(filepath)
			case <- quit:
				ticker.Stop()
				return
			}
		}
	}()

	return nil
}

func (m *Manager) GetNumberOfProxy() (int) {
	return len(m.proxies)
}

func (m *Manager) GetProxy() (*Proxy, error) {
	rand.Seed(time.Now().UTC().UnixNano())
	r := rand.Intn(len(m.proxies))
	return m.proxies[r], nil
}

func (m *Manager) readProxiesFromFile(filepath string) (error) {
	file, err := ioutil.ReadFile(filepath)
	if err != nil {
		return err
	}

	//var proxies []Proxy
	var values []map[string]interface{}

	err = json.Unmarshal([]byte(file), &values)
	if err != nil {
		return err
	}

	m.proxies = nil
	for _, data := range values {

		infos := make(map[string]string, 6)
		for k, v := range data {
			if k == "port" {
				infos["port"] = fmt.Sprintf("%v", v)
			} else {
				infos[strings.ToLower(k)] = strings.ToLower(v.(string))
			}
		}

		var addrs []string

		if _, ok := infos["domain"]; ok {
			lookedUpAddrs, err := net.LookupHost(infos["domain"])
			if err != nil {
				return err
			}
			for _, data := range lookedUpAddrs {
				addrs = append(addrs, data)
			}
		} else {
			addrs = append(addrs, infos["ipaddress"])
		}

		for _, data := range addrs {
			addr, err := net.ResolveTCPAddr("tcp", fmt.Sprintf("%s:%s", data, infos["port"]))
			if err != nil {
				return err
			}

			p := Proxy{
				id:    0,
				addr:  addr,
				infos: infos,
			}
			m.proxies = append(m.proxies, &p)
		}
	}

	return nil
}
